package com.example.hazama.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.hazama.domain.Customer;
import com.example.hazama.domain.CustomerFriend;
import com.example.hazama.domain.CustomerLoginAndLogout;
import com.example.hazama.domain.CustomerSave;
import com.example.hazama.repository.AddPointRepository;
import com.example.hazama.repository.FriendRegisterAndRemoveRepository;
import com.example.hazama.repository.CustomerGetAndRegisterRepository;
import com.example.hazama.repository.GetMonsterRepository;
import com.example.hazama.repository.LoginAndLogoutRepository;

@Service
@Transactional
public class CustomerService {	 	

//---------------------------ユーザー情報一件取得
		@Autowired
		CustomerGetAndRegisterRepository customerGetAndRegisterRepository;
			
		public Customer getCustomer(String email) {
				return customerGetAndRegisterRepository.getCustomer(email);
		}
//---------------------------ユーザー情報新規登録					
		public CustomerSave customerRegister(String email,String password,String nickName,int age) {
					return customerGetAndRegisterRepository.customerRegister(email,password,nickName,age);
		}
//---------------------------ログインログアウト
		@Autowired
		LoginAndLogoutRepository LoginAndLogoutRepository;
	 	
	    public CustomerLoginAndLogout customerLogout(String email,String password){
	    	return LoginAndLogoutRepository.customerLogout(email, password);
	    }
		 
	    public CustomerLoginAndLogout customerLogin(String email,String password){
	    	return LoginAndLogoutRepository.customerLogin(email, password);
	    }
	    
//---------------------------所持モンスター全件取得
	    @Autowired
	 	GetMonsterRepository getMonsterRepository;
	    
	    public String getMonster(String monster) {
			return getMonsterRepository.getMonster(monster);
		}
	    
//---------------------------ポイント加算
		@Autowired
		AddPointRepository addPointRepository;
		
		public Customer addPoint(String email,int point) {
				return addPointRepository.addPoint(email,point);
		}
//-----------------------------フレンド全件取得
		@Autowired
		FriendRegisterAndRemoveRepository friendRegisterAndRemoveRepository;
		
		public List<CustomerFriend> getAllFriends(String email) {
			return friendRegisterAndRemoveRepository.getAllFriends(email);
	}
//----------------------------フレンド一件登録
		public List<CustomerFriend> registerFriend(String email,String friendEmail) {
			return friendRegisterAndRemoveRepository.registerFriend(email,friendEmail);
	}
//----------------------------フレンド一件削除
		public CustomerFriend removeFriend(String email,String friendEmail) {
			return friendRegisterAndRemoveRepository.removeFriend(email,friendEmail);
	}
}
