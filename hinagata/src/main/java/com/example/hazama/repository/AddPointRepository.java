package com.example.hazama.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.hazama.domain.Customer;

@Repository
public class AddPointRepository {
		@Autowired
		NamedParameterJdbcTemplate jdbcTemplate;
	
		Customer c;
//--------------------------------ポイント加算	
	 @Transactional
	 public Customer addPoint(String email,int point) {

			SqlParameterSource  param = new MapSqlParameterSource().addValue("email", email)
																												.addValue("point", point);
			try {

				jdbcTemplate.update("UPDATE user_basic SET   Point = Point + :point WHERE Email = :email", param);
				// Customerオブジェクトにクエリの結果をマッピングする
				c = jdbcTemplate.queryForObject( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
						"SELECT * FROM user_basic WHERE Email = :email", param,
						new RowMapper<Customer>() {
							@Override
							public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
								return new Customer(rs.getString("Email"), rs.getString("Password"),
										rs.getString("NickName"), rs.getInt("Point"), 
										rs.getInt("Rank"),rs.getInt("Age"), 
										new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format( rs.getTimestamp("EntryDate")), 
										new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format( rs.getTimestamp("LastDateTime")),
										rs.getInt("LoginState"));
							}
						});
				return c;
				
			} catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				System.out.println("そのアカウントはありません。");
				return null;
			}
	 }
}
