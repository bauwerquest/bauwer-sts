package com.example.hazama.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.hazama.domain.CustomerFriend;

@Repository
public class FriendRegisterAndRemoveRepository {
			@Autowired
			NamedParameterJdbcTemplate jdbcTemplate;
			
			List<CustomerFriend> cf;
//--------------------------------フレンド全件取得		
			 @Transactional
			 public List<CustomerFriend> getAllFriends(String email) {

					SqlParameterSource  param = new MapSqlParameterSource().addValue("email", email);
					try {
//						user_basic にログインユーザーのアカウントが存在しているか email を使ってチェックして、存在していなかったら正常にnull を返す。					
						int yet=jdbcTemplate.queryForObject("SELECT COUNT(*) FROM user_basic WHERE EXISTS (SELECT * FROM user_basic WHERE Email = :email)", param,Integer.class);
						if(yet<1) return null;
						
//						存在していたら、user_friend のEmail カラムがログインユーザーの email であるレコードを全件取得する。
//						ドメイン CustomerFriend 型の List<CustomerFriend> を返す。
						
						// Customerオブジェクトにクエリの結果をマッピングする
						cf = jdbcTemplate.query( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
								"SELECT * FROM user_friend WHERE Email = :email", param,
								new RowMapper<CustomerFriend>() {
									@Override
									public CustomerFriend mapRow(ResultSet rs, int rowNum) throws SQLException {
										return new CustomerFriend(rs.getString("Email"),rs.getString("FriendEmail"));
									}
								});
						return cf;
						
					} catch (EmptyResultDataAccessException e) {
						e.printStackTrace();
						System.out.println("フレンドはありません。");
						cf.clear();
						return cf;
					}
			 }
//---------------------------------------フレンド一件登録			 
			 @Transactional
			 public List<CustomerFriend> registerFriend(String email,String friendEmail) {

					SqlParameterSource  param = new MapSqlParameterSource().addValue("email", email)
																														.addValue("friendEmail", friendEmail);

					int count;
					
					try {
//						フレンドを登録する前に以下項目をのチェックする。
//						1.user_basic にログインユーザーのアカウントが存在しているか。
						count = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM user_basic WHERE Email = :email",param, Integer.class);
						if(count<1) {
							cf.clear();
							return cf;
						}
//						2.user_basic にフレンド登録しようとしているアカウントが存在しているか。
//						3.ログインユーザーのEmail をフレンド登録しようとしていないか。
						count= jdbcTemplate.queryForObject("SELECT COUNT(*) FROM user_basic WHERE Email = :friendEmail",param, Integer.class);
						if(count<1||(email==friendEmail)) {
							cf.clear();
							return cf;
						}
//						4.ログインユーザーのフレンドは5人未満か
						count= jdbcTemplate.queryForObject("SELECT COUNT(*) FROM user_friend WHERE Email = :email",param, Integer.class);
						if(count>=5) {
							cf.clear();
							return cf;
						}
//						条件の全てを満たしていた場合は user_friend にログインユーザーの emailと登録したいフレンドの friendEmailをテーブルに追加する。
//						このとき、追加しようとしているレコードが user_friend に存在する（すでにフレンドだった）のならば、
//						空のList<CustomerFriend> を返す。
						try {

							int friend=jdbcTemplate.queryForObject("SELECT COUNT(*) FROM  user_friend WHERE Email=:email"+
							" AND FriendEmail=:friendEmail", param,Integer.class);
							
							if(friend>0) {
								System.out.println("すでにフレンド登録されています");
								cf.clear();
								return cf;
							}else {
								jdbcTemplate.update("INSERT INTO user_friend(Email,FriendEmail)"+
										"VALUES (:email,:friendEmail);", param);
							}	
							
						} catch (EmptyResultDataAccessException e) {
							e.printStackTrace();
							System.out.println("エラーが発生しました");
							cf.clear();
							return cf;
						}
	
//						登録が成功した場合、登録後の user_friend の Email カラムがログインユーザーの email であるものを全件取得し
//						List<CustomerFriend> に値を代入して返す。
						return getAllFriends(email);
						
					} catch (EmptyResultDataAccessException e) {
						e.printStackTrace();
						System.out.println("フレンドはありません。");
						cf.clear();
						return cf;
					}				
			 }
//---------------------------------------フレンド一件登削除
			 @Transactional
			 public CustomerFriend removeFriend(String email,String friendEmail) {
				 
					SqlParameterSource  param = new MapSqlParameterSource().addValue("email", email)
																														.addValue("friendEmail", friendEmail);
					try {
//				 user_friendにログインユーザーのemail、削除したいフレンドのfriendEmailの
//				 組み合わせになっているレコードが存在しているかチェックする。
					int friend=jdbcTemplate.queryForObject("SELECT COUNT(*) from user_friend WHERE Email=:email"+
					" AND FriendEmail=:friendEmail", param,Integer.class);
//				 存在する場合、そのレコードを削除して削除したレコードの情報を返す。
					if(friend>0) {
						try {
							jdbcTemplate.update("DELETE FROM user_friend WHERE Email=:email"+
									" AND FriendEmail=:friendEmail", param);
							
						} catch (EmptyResultDataAccessException e) {
							e.printStackTrace();
							System.out.println("エラーが発生しました");
							return null;
						}				
					}else {
//						 存在しない場合、正常にnullを返す。
						System.out.println("フレンドがありません");
						return null;
					}
					System.out.println("フレンド削除完了");
					return new CustomerFriend(email,friendEmail);

					} catch (EmptyResultDataAccessException e) {
						e.printStackTrace();
						System.out.println("エラーが発生しました");
						return null;
					}				
			 }
}
