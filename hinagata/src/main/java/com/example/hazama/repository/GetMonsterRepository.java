package com.example.hazama.repository;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public class GetMonsterRepository {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;
	
	String monster;
//---------------------------------所持モンスター全件取得
    @Transactional
	public String getMonster(String email) {

		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email);

		try {
			monster = jdbcTemplate.queryForObject( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
					"SELECT Name FROM user_monster WHERE Email = :email", param,
					new RowMapper<String>() {
						@Override
						public String mapRow(ResultSet rs, int rowNum) throws SQLException {
							return rs.getString("Name");
						}
					});

		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("モンスターがありません");
			return null;
		}
		return monster;
	}
}
