package com.example.hazama.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.hazama.domain.CustomerLoginAndLogout;

@Repository
@Transactional
public class LoginAndLogoutRepository {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	CustomerLoginAndLogout c;
//---------------------------------------ログイン
	public CustomerLoginAndLogout customerLogin(String email, String password) {
//    	クライアントから渡されたデータをCustomerLoginAndLogoutで受け取りuser_basicにemailとpasswordで検索をかける。
//    	ユーザーのアカウントが存在していなかったら、正常にnullを返す。
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email).addValue("password", password).addValue("LoginState", 1);

		try {
			c = jdbcTemplate.queryForObject( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
					"SELECT * FROM user_basic WHERE Email = :email AND Password=:password", param,
					new RowMapper<CustomerLoginAndLogout>() {
						@Override
						public CustomerLoginAndLogout mapRow(ResultSet rs, int rowNum) throws SQLException {
							return new CustomerLoginAndLogout(rs.getString("Email"), rs.getString("Password"),
									rs.getString("NickName"), rs.getInt("LoginState"));
						}
					});
//    	LoginStateが0か1以外だったら不正とし、正常にnullを返す。
			if (c.getLoginState() != 0 && c.getLoginState() != 1) return null;
//    	ユーザーがすでにログイン状態かログアウト状態なら、
//    	LoginStateをログイン状態を示す「1」に上書きする。

				//param = new MapSqlParameterSource().addValue("LoginState", 1);

				try {

					jdbcTemplate.update("UPDATE user_basic SET LoginState=:LoginState "+ "WHERE Email = :email", param);
					// Customerオブジェクトにクエリの結果をマッピングする
					c = jdbcTemplate.queryForObject( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
							"SELECT * FROM user_basic WHERE Email = :email", param,
							new RowMapper<CustomerLoginAndLogout>() {
								@Override
								public CustomerLoginAndLogout mapRow(ResultSet rs, int rowNum) throws SQLException {
									return new CustomerLoginAndLogout(rs.getString("Email"), rs.getString("Password"),
											rs.getString("NickName"), rs.getInt("LoginState"));
								}
							});

					return c;
				} catch (EmptyResultDataAccessException e) {
					e.printStackTrace();
					System.out.println("そのアカウントはありません。");
					return null;
				}
			// 処理の結果はCustomerLoginAndLogoutに代入して返す

		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません。");
			return null;
		}
	}
	
//----------------------------------ログアウト
	public CustomerLoginAndLogout customerLogout(String email, String password) {
//    	クライアントから渡されたデータを ドメイン「CustomerLoginAndLogout 」で受け取り、
//    	ログインユーザーのemail を使って user_basic にアカウント情報が存在するかチェックする。
		SqlParameterSource param = new MapSqlParameterSource().addValue("email", email).addValue("password", password);

		try {
			c = jdbcTemplate.queryForObject( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
					"SELECT * FROM user_basic WHERE Email = :email", param, new RowMapper<CustomerLoginAndLogout>() {
						@Override
						public CustomerLoginAndLogout mapRow(ResultSet rs, int rowNum) throws SQLException {
							return new CustomerLoginAndLogout(rs.getString("Email"), rs.getString("Password"),
									rs.getString("NickName"), rs.getInt("LoginState"));
						}
					});
//    	 	      ユーザーのアカウントが存在する場合以下の処理を実行する。 LoginState がログアウト状態を示す「0 」か、
//    	 	      ログイン状態示す「1」かチェックする。 →いずれとも異なる値だった場合：不正な状態として正常にnullを返す。
			if (c.getLoginState() != 0 && c.getLoginState() != 1)return null;

//    	 	     →正常なログイン状態だった場合：最終ログアウト日時 ( )を現在時刻に上書きする
//    	 	    	「1LoginState 」を「 0 」に上書きして現在のログイン状態をログアウト状態にする。
//       	 	     →既にログアウト済みだった場合：値の変更は行わない。
			if (c.getLoginState() == 1) {

				//Date d=new Date();
			     //SimpleDateFormat sdf = new SimpleDateFormat("yyyy'-'MM'-'dd' 'HH':'mm':'ss");
		        //Timestamp d = new Timestamp(System.currentTimeMillis());
				 //LocalDateTime d = LocalDateTime.now();
				//Calendar d = Calendar.getInstance();

				param = new MapSqlParameterSource().addValue("LoginState", 0).addValue("email", email);
				
				try {

					jdbcTemplate.update("UPDATE user_basic SET LoginState=:LoginState," 
					+"LastDateTime=now()"+ " WHERE Email = :email", param);
					// Customerオブジェクトにクエリの結果をマッピングする
					c = jdbcTemplate.queryForObject( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
							"SELECT * FROM user_basic WHERE Email = :email", param,
							new RowMapper<CustomerLoginAndLogout>() {
								@Override
								public CustomerLoginAndLogout mapRow(ResultSet rs, int rowNum) throws SQLException {
									return new CustomerLoginAndLogout(rs.getString("Email"), rs.getString("Password"),
											rs.getString("NickName"), rs.getInt("LoginState"));
								}
							});

					return c;
				} catch (EmptyResultDataAccessException e) {
					e.printStackTrace();
					System.out.println("そのアカウントはありません。");
					return null;
				}
			}
//    	 	    	処理の結果を CustomerLoginAndLogout に値を代入して返す。 ユーザーのアカウントが存在していない場合、正常にnull を返す。
			return c;
		} catch (EmptyResultDataAccessException e) {
			e.printStackTrace();
			System.out.println("そのアカウントはありません。");
			return null;
		}
	}
}
