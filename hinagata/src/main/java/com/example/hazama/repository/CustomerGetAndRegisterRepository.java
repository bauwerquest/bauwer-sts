package com.example.hazama.repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.hazama.domain.Customer;
import com.example.hazama.domain.CustomerSave;

@Repository
public class CustomerGetAndRegisterRepository {
	@Autowired
	NamedParameterJdbcTemplate jdbcTemplate;

	Customer c;
//-------------------------------ユーザー情報一件取得
	 @Transactional
	 public Customer getCustomer(String email) {

			SqlParameterSource  param = new MapSqlParameterSource().addValue("email", email);
			try {
//				クライアントから渡された「email」で「user_basic」を検索し一致したユーザー情報のレコード(横一行のこと)を一件取得する。
//				取得した値を保持させるドメイン「Customer」を用意する。
//				値が取得できたら、Customerに値を代入して返す。

				// Customerオブジェクトにクエリの結果をマッピングする
				c = jdbcTemplate.queryForObject( // queryForObjectは『SQL文』、『パラメータ』、『戻り値のクラス』を指定します
						"SELECT * FROM user_basic WHERE Email = :email", param,
						new RowMapper<Customer>() {
							@Override
							public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
								return new Customer(rs.getString("Email"), rs.getString("Password"),
										rs.getString("NickName"), rs.getInt("Point"), 
										rs.getInt("Rank"),rs.getInt("Age"), 
										new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format( rs.getTimestamp("EntryDate")), 
										new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format( rs.getTimestamp("LastDateTime")),
										rs.getInt("LoginState"));
							}
						});
				return c;
//				検索が失敗してレコードを取得できなかったら正常にnullを返す。(HTTPステータス「200OK」で返ってくること。)
			} catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				System.out.println("そのアカウントはありません。");
				return null;
			}
	 }
//-----------------------------------ユーザー情報新規登録
	 @Transactional
	 public CustomerSave customerRegister(String email,String password,String nickName,int age) {
		 	
		 	Date d=new Date();
			SqlParameterSource  param = new MapSqlParameterSource().addValue("email", email).addValue("password", password)
																															.addValue("nickName", nickName).addValue("age", age)
																															.addValue("zero", 0).addValue("registerDate", d)
																															.addValue("basicMonster", "elf");
			try {
				
//			クライアントから渡された情報(email, password, nickname, age)をもとに、user_basicにユーザーを登録する処理を用意する。
//			その時に渡されたemailが登録されていないかチェックをする。されていなければ登録成功として DBにユーザーを登録する。
				int isResisted=jdbcTemplate.queryForObject("SELECT COUNT(*)  FROM user_basic WHERE  Email=:email", param,Integer.class);
				
				if(isResisted>0) {
					System.out.println("このメールアドレスは登録済みです。");
					return null;
				}
				
//			登録成功時、受け取ったユーザー情報以外の項目は以下のように設定する。
//			1、EntryDate,LastDateTimeは登録した時の日時を入れる。
//			2、Point,Rank,LoginStateは初期値として全て0を入れる。
//			3、user_monsterに新規登録したユーザーのEmailと初期モンスターとしてelfを登録する。
				jdbcTemplate.update("INSERT INTO user_basic(Email,Password,NickName,Point,Rank,Age,EntryDate,LastDateTime,LoginState)"+
						"VALUES (:email,:password,:nickName,:zero,:zero,:age,:registerDate,:registerDate,:zero);", param);
				
				jdbcTemplate.update("INSERT INTO user_monster(Email,Name)"+
						"VALUES (:email,:basicMonster);", param);
				
//			ドメイン「CustomerSave」を作成し、そこに値を返す。
				return new CustomerSave(email,password,nickName,age);
//			登録失敗時（すでに登録済み）のときsaveエラーとして、正常にnullを返す
			} catch (EmptyResultDataAccessException e) {
				e.printStackTrace();
				System.out.println("エラーが発生しました。");
				return null;
			}
	 }
}
