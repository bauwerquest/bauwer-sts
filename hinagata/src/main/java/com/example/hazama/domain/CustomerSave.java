package com.example.hazama.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//入力受け付け用beans
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerSave {
	private String email;
	private String password;
	private String nickName;
	private int age;
	//sample
}
