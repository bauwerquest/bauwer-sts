package com.example.hazama.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//入力受け付け用beans
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CustomerMailPass {
	private String email;
	private String password;
}
