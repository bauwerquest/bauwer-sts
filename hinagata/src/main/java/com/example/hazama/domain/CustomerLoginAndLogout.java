package com.example.hazama.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data//getter/setter
@AllArgsConstructor//RowMapperで使用
@NoArgsConstructor
public class CustomerLoginAndLogout {
			private String email;
			private String password;
			private String nickname;
			private int    loginState;
}
