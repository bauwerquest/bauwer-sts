package com.example.hazama.api;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.hazama.domain.CustomerMailPass;
import com.example.hazama.domain.CustomerSave;
import com.example.hazama.domain.Customer;
import com.example.hazama.domain.CustomerFriend;
import com.example.hazama.domain.CustomerLoginAndLogout;
import com.example.hazama.service.CustomerService;

@RestController
@RequestMapping("customerapi") //リクエストの際のルートパス
public class CostomerRestController {
	
	@Autowired
	CustomerService customerService;
	
//---------------------------ユーザー情報一件取得
		@RequestMapping(value =  "{email}/find ",method = RequestMethod.GET)//今回のは方式はGET　http://localhost:8080/customerapi/point/int型の数字
		Customer getCustomers(@PathVariable String  email){//URLの中の{}の中の同名のパラメーターをPathVariableの中の同名の型に代入
			Customer  customer = customerService.getCustomer(email);//メソッドの実行
			return customer; //これがJSON形式でレスポンスになる
}
//---------------------------ユーザー情報新規登録
		@RequestMapping(value =  "/save",method = RequestMethod.POST)
		CustomerSave customerRegister(@RequestBody CustomerSave cs){//Bodyに入れた情報の型と同じ型しか入らない Post版PathVariable
			CustomerSave customerSave = customerService.customerRegister(cs.getEmail(), cs.getPassword(),cs.getNickName(),cs.getAge());//メソッドの呼び出し
			return customerSave; //これがJSON形式でレスポンスになる
		}
//---------------------------ログインログアウト
	//customerapi/change/logoutにPOSTでアクセスするとcustomerLogoutメソッドが呼ばれる
		@RequestMapping(value =  "/logout",method = RequestMethod.POST)
		CustomerLoginAndLogout customerLogout(@RequestBody CustomerMailPass cmp){//Bodyに入れた情報の型と同じ型しか入らない Post版PathVariable
			CustomerLoginAndLogout customerLoginAndLogout = customerService.customerLogout(cmp.getEmail(), cmp.getPassword());//メソッドの呼び出し
			return customerLoginAndLogout; //これがJSON形式でレスポンスになる
		}
		
		@RequestMapping(value =  "/login",method = RequestMethod.POST)
		CustomerLoginAndLogout customerLogin(@RequestBody CustomerMailPass cmp){//Bodyに入れた情報の型と同じ型しか入らない Post版PathVariable
			CustomerLoginAndLogout customerLoginAndLogout = customerService.customerLogin(cmp.getEmail(), cmp.getPassword());//メソッドの呼び出し
			return customerLoginAndLogout; //これがJSON形式でレスポンスになる
		}
	
//---------------------------所持モンスター全件取得
		@RequestMapping(value =  "{email}/monster",method = RequestMethod.GET)//今回のは方式はGET　http://localhost:8080/customerapi/point/int型の数字
			String getMonster(@PathVariable String  email){//URLの中の{}の中の同名のパラメーターをPathVariableの中の同名の型に代入
				String  monster = customerService.getMonster (email);//メソッドの実行
				return monster; //これがJSON形式でレスポンスになる
		}
//---------------------------ポイント加算
		@RequestMapping(value =  "{email}/{point}/point",method = RequestMethod.GET)//今回のは方式はGET　http://localhost:8080/customerapi/point/int型の数字
		Customer addPoint(@PathVariable String  email ,@PathVariable int point){//URLの中の{}の中の同名のパラメーターをPathVariableの中の同名の型に代入
			Customer  customer = customerService.addPoint (email,point);//メソッドの実行
			return customer; //これがJSON形式でレスポンスになる
	}
//----------------------------フレンド全件取得
		@RequestMapping(value =  "{email}/friend ",method = RequestMethod.GET)//今回のは方式はGET　http://localhost:8080/customerapi/point/int型の数字
		List<CustomerFriend> getAllFriends(@PathVariable String  email){//URLの中の{}の中の同名のパラメーターをPathVariableの中の同名の型に代入
			List<CustomerFriend> customerFriendList = customerService.getAllFriends (email);//メソッドの実行
			return customerFriendList; //これがJSON形式でレスポンスになる
	}
//----------------------------フレンド一件登録
		@RequestMapping(value =  "{email}/{friendEmail}/friend" ,method = RequestMethod.GET)//今回のは方式はGET　http://localhost:8080/customerapi/point/int型の数字
		List<CustomerFriend> registerFriend(@PathVariable String  email,@PathVariable String friendEmail){//URLの中の{}の中の同名のパラメーターをPathVariableの中の同名の型に代入
			List<CustomerFriend> customerFriendList = customerService.registerFriend (email,friendEmail);//メソッドの実行
			return customerFriendList; //これがJSON形式でレスポンスになる
	}
//----------------------------フレンド一件削除
		@RequestMapping(value =  "{email}/{friendEmail}/deleteFriend" ,method = RequestMethod.GET)//今回のは方式はGET　http://localhost:8080/customerapi/point/int型の数字
		CustomerFriend removeFriend(@PathVariable String  email,@PathVariable String friendEmail){//URLの中の{}の中の同名のパラメーターをPathVariableの中の同名の型に代入
			CustomerFriend customerFriend = customerService.removeFriend (email,friendEmail);//メソッドの実行
			return customerFriend; //これがJSON形式でレスポンスになる
	}
}
